**Pour exécuter le serveur :**
    Il existe deux scripts à la racine du projet : "_goBuild.bat_" pour nettoyer/compiler le projet sous windows, et "_gobuild.sh_" pour nettoyer/compiler le projet sous linux.

**Tests :**
    Les tests s'exécutent en parallèle, ce qui fausse souvent leur résultats. Pour les faire réussir, il faut les exécuter un à un séquentiellement (par exemple sous vscode avec l'extension go, il est possible d'exécuter les tests un par un), et bien veiller à démarrer le serveur.

**Provider :**
    La variable UseMemory du Provider (_internal/persistence/provider_) détermine si le DAO utilise le _DAOMem_ ou le _DAOBolt_. Par défaut celle-ci est à false, donc c'est le bolt qui est utilisé.

**Swagger :**
    L'utilisation de Swagger pour l'auto génération de la documentation ne fonctionne pas, pourtant j'ai suivi les instructions d'un tutoriel et je ne comprends pas pourquoi cela ne marche pas.
