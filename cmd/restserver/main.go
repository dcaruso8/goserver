package main

import (
	_ "cmd/restserver/docs"
	"internal/web/rest"
	"net/http"

	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
)

// @title Student & Language API documentation
// @version 1.0.0
// @host localhost:8080
// @BasePath /

func main() {
	//handlers
	r := mux.NewRouter()

	//GET
	r.HandleFunc("/languages", rest.GetAllLanguages).Methods("Get")
	r.HandleFunc("/languages/{code}/", rest.GetLanguageByCode).Methods("Get")

	r.HandleFunc("/students", rest.GetAllStudents).Methods("Get")
	r.HandleFunc("/students/{id}/", rest.GetStudentById).Methods("Get")

	//POST
	r.HandleFunc("/languages", rest.CreateLanguage).Methods("POST")
	r.HandleFunc("/students", rest.CreateStudent).Methods("POST")

	//PUT
	r.HandleFunc("/languages", rest.UpdateLanguage).Methods("PUT")
	r.HandleFunc("/students", rest.UpdateStudent).Methods("PUT")

	//DELETE
	r.HandleFunc("/languages/{code}/", rest.DeleteLanguageByCode).Methods("DELETE")
	r.HandleFunc("/students/{id}/", rest.DeleteStudentById).Methods("DELETE")

	//swagger
	r.PathPrefix("/swagger").Handler(httpSwagger.WrapHandler)

	http.ListenAndServe(":8080", r)
}
