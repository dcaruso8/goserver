module Projet

go 1.15

require github.com/gorilla/mux v1.8.0

//INTERNAL MODULES (with REPLACE)
require internal/entities v1.0.0

replace internal/entities => ./internal/entities

require internal/persistence v1.0.0

replace internal/persistence => ./internal/persistence

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
	internal/web/rest v1.0.0
)

replace internal/web/rest => ./internal/web/rest

require internal/persistence/bolt v1.0.0

replace internal/persistence/bolt => ./internal/persistence/bolt

require internal/persistence/daoBolt v1.0.0

replace internal/persistence/daoBolt => ./internal/persistence/daoBolt

require (
	github.com/google/go-cmp v0.5.7 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	internal/persistence/daoMem v1.0.0
)

replace internal/persistence/daoMem => ./internal/persistence/daoMem

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/go-openapi/spec v0.20.5 // indirect
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/http-swagger v1.2.6 // indirect
	github.com/swaggo/swag v1.8.1 // indirect
	github.com/urfave/cli/v2 v2.4.0 // indirect
	golang.org/x/net v0.0.0-20220420153159-1850ba15e1be // indirect
	golang.org/x/tools v0.1.10 // indirect
	internal/persistence/provider v1.0.0
)

replace internal/persistence/provider => ./internal/persistence/provider

require (
	cmd/restserver/docs v1.0.0
	github.com/swaggo/gin-swagger v1.4.2 // indirect
)

replace cmd/restserver/docs => ./cmd/restserver/docs
