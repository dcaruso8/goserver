package entities

import "fmt"

type Language struct {
	Code string
	Name string
}

func NewLanguage(code string, name string) Language {
	newLanguage := Language{code, name}
	return newLanguage
}

func (l Language) String() string {
	return fmt.Sprintf("Language %s with code %s", l.Name, l.Code )
}
