package entities

import "fmt"

type Student struct {
	Id           int
	FirstName    string
	LastName     string
	Age          int
	LanguageCode string
}

func NewStudent(id int, firstName string, lastName string, age int, languageCode string) Student {
	newStudent := Student{id, firstName, lastName, age, languageCode}
	return newStudent
}

func (s Student) String() string {
	return fmt.Sprintf("Student number %d, named %s %s, %d years old. Language code : %s", s.Id, s.FirstName, s.LastName, s.Age, s.LanguageCode)
}
