package bolt

import (
	"log"

	"github.com/boltdb/bolt"
)

type MyBolt struct {
	db *bolt.DB
}

//var daoBolt MyBolt
var database *bolt.DB = nil

func DbOpen(fileName string) MyBolt {
	if database != nil {
		return MyBolt{db: database}
	} else {
		dbObj, err := bolt.Open(fileName, 0600, nil)
		if err != nil {
			log.Fatal(err)
		}

		database = dbObj
		return MyBolt{db: database}
	}
}

func (db MyBolt) DbClose() {
	db.db.Close()
}

func (db MyBolt) DbPath() string {
	return db.db.Path()
}

func (db MyBolt) CreateBucket(bucketName string) {
	err := db.db.Update(func(tx *bolt.Tx) error {
		//deleting bucket if already exists
		bucket := tx.Bucket([]byte(bucketName))
		if bucket != nil {
			tx.DeleteBucket([]byte(bucketName))
		}
		_, err := tx.CreateBucketIfNotExists([]byte(bucketName))
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}

func (db MyBolt) DbPut(bucketName string, key string, value string) {
	err := db.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}
		bucket.Put([]byte(key), []byte(value))
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}

func (db MyBolt) DbGet(bucketName string, key string) string {
	var value string
	err := db.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}
		value = string(bucket.Get([]byte(key)))
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	return value
}

func (db MyBolt) DbGetAll(bucketName string) []string {
	var values []string
	err := db.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}
		bucket.ForEach(func(k, v []byte) error {
			values = append(values, string(v))
			return nil
		})

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	return values
}

func (db MyBolt) DbDelete(bucketName string, key string) {
	err := db.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		if bucket == nil {
			panic("Bucket " + bucketName + " not found")
		}
		bucket.Delete([]byte(key))
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}
