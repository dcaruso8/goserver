package daoBolt

import (
	"encoding/json"
	"internal/entities"
	"internal/persistence/bolt"
)

type LanguageDAOBolt struct{}

var languageDAOBolt LanguageDAOBolt

var languageDB bolt.MyBolt

func NewLanguageDAOBolt() LanguageDAOBolt {
	start()
	return languageDAOBolt
}

func start() {
	languageDB = bolt.DbOpen("database.db")

	//creating bucket
	languageDB.CreateBucket("languageBucket")

	//inserting data
	var languages []entities.Language = []entities.Language{
		entities.NewLanguage("Fr", "Français"), entities.NewLanguage("En", "Anglais"),
	}

	for _, language := range languages {
		res, _ := json.Marshal(language) //convert to json
		languageString := string(res)    //convert json to string
		code := language.Code

		languageDB.DbPut("languageBucket", code, languageString)
	}
}

//------------------------------------------------

func (dao LanguageDAOBolt) FindAll() []entities.Language {
	languagesString := languageDB.DbGetAll("languageBucket")
	var languages []entities.Language

	for _, languageString := range languagesString {
		var language entities.Language
		json.Unmarshal([]byte(languageString), &language)

		languages = append(languages, language)
	}

	return languages
}

func (dao LanguageDAOBolt) Find(code string) *entities.Language {
	languageString := languageDB.DbGet("languageBucket", code)
	if languageString != "" {
		var language entities.Language
		json.Unmarshal([]byte(languageString), &language)
		return &language
	}
	return nil
}

func (dao LanguageDAOBolt) Exists(code string) bool {
	languageString := languageDB.DbGet("languageBucket", code)
	if languageString != "" {
		return true
	}
	return false
}

func (dao LanguageDAOBolt) Delete(code string) bool {
	if dao.Exists(code) {
		languageDB.DbDelete("languageBucket", code)
		return true
	}
	return false
}

func (dao LanguageDAOBolt) Create(language entities.Language) bool {
	if dao.Exists(language.Code) {
		return false
	}

	res, _ := json.Marshal(language) //convert to json
	languageString := string(res)    //convert json to string
	languageDB.DbPut("languageBucket", language.Code, languageString)
	return true
}

func (dao LanguageDAOBolt) Update(language entities.Language) bool {
	if dao.Exists(language.Code) {
		res, _ := json.Marshal(language) //convert to json
		languageString := string(res)    //convert json to string
		languageDB.DbPut("languageBucket", language.Code, languageString)
		return true
	}

	return false
}
