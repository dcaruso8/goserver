package daoBolt

import (
	"encoding/json"
	"internal/entities"
	"internal/persistence/bolt"
	"sort"
	"strconv"
)

type StudentDAOBolt struct{}

var studentDB bolt.MyBolt
var studentDaoBolt StudentDAOBolt

func NewStudentDAOBolt() StudentDAOBolt {
	Start()
	return studentDaoBolt
}

func Start() {
	studentDB = bolt.DbOpen("database.db")

	//creating bucket
	studentDB.CreateBucket("studentBucket")

	//inserting data
	var students []entities.Student = []entities.Student{
		entities.NewStudent(1, "Gaspard", "Missiaen", 21, "23"),
		entities.NewStudent(2, "Daurian", "Gauron", 20, "Go"),
		entities.NewStudent(3, "Daryl", "Caruso", 20, "-2"),
		entities.NewStudent(4, "Christopher", "Lessirard", 20, "26"),
		entities.NewStudent(5, "Nathan", "Da Silva Neves", 20, "26"),
	}

	for _, student := range students {
		res, _ := json.Marshal(student) //convert to json
		studentString := string(res)    //convert json to string
		studentDB.DbPut("studentBucket", strconv.Itoa(student.Id), studentString)
	}
}

//------------------------------------------------

func (dao StudentDAOBolt) FindAll() []entities.Student {
	studentsString := studentDB.DbGetAll("studentBucket")
	var students []entities.Student

	for _, studentString := range studentsString {
		var student entities.Student
		json.Unmarshal([]byte(studentString), &student)

		students = append(students, student)
	}

	sort.SliceStable(students, func(i, j int) bool {
		return students[i].Id < students[j].Id
	})

	return students
}

func (dao StudentDAOBolt) Find(id int) *entities.Student {
	studentString := studentDB.DbGet("studentBucket", string(id))
	if studentString != "" {
		var student entities.Student
		json.Unmarshal([]byte(studentString), &student)
		return &student
	}
	return nil
}

func (dao StudentDAOBolt) Exists(id int) bool {
	student := studentDB.DbGet("studentBucket", string(id))
	if student != "" {
		return true
	}
	return false
}

func (dao StudentDAOBolt) Delete(id int) bool {
	if dao.Exists(id) {
		studentDB.DbDelete("studentBucket", string(id))
		return true
	}
	return false
}

func (dao StudentDAOBolt) Create(student entities.Student) bool {
	if dao.Exists(student.Id) {
		return false
	}

	res, _ := json.Marshal(student) //convert to json
	studentString := string(res)    //convert json to string
	studentDB.DbPut("studentBucket", string(student.Id), studentString)
	return true
}

func (dao StudentDAOBolt) Update(student entities.Student) bool {
	if dao.Exists(student.Id) {
		res, _ := json.Marshal(student) //convert to json
		studentString := string(res)    //convert json to string
		studentDB.DbPut("studentBucket", string(student.Id), studentString)
		return true
	}

	return false
}
