package daoMem

import (
	"internal/entities"
)

type LanguageDAOMem struct{}

var languageDao LanguageDAOMem

func NewLanguageDAOMem() LanguageDAOMem {
	return languageDao
}

var languages []entities.Language = []entities.Language{
	entities.NewLanguage("Fr", "Français"), entities.NewLanguage("En", "Anglais"),
}

func (dao LanguageDAOMem) FindAll() []entities.Language {
	return languages
}

func (dao LanguageDAOMem) Find(code string) *entities.Language {
	for _, lang := range languages {
		if lang.Code == code {
			return &lang
		}
	}
	return nil
}

func (dao LanguageDAOMem) Exists(code string) bool {
	for _, lang := range languages {
		if lang.Code == code {
			return true
		}
	}
	return false
}

func (dao LanguageDAOMem) Delete(code string) bool {
	for index, lang := range languages {
		if lang.Code == code {
			languages = append(languages[:index], languages[index+1:]...)
			return true
		}
	}
	return false
}

func (dao LanguageDAOMem) Create(language entities.Language) bool {
	if dao.Exists(language.Code) {
		return false
	}
	languages = append(languages, language)
	return true
}

func (dao LanguageDAOMem) Update(language entities.Language) bool {
	for index, lang := range languages {
		if lang.Code == language.Code {
			languages[index] = language
			return true
		}
	}
	return false
}
