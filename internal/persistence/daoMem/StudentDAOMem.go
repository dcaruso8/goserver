package daoMem

import (
	"internal/entities"
	"sort"
)

type StudentDAOMem struct{}

var studentDao StudentDAOMem

func NewStudentDAOMem() StudentDAOMem {
	return studentDao
}

var students []entities.Student = []entities.Student{
	entities.NewStudent(1, "Gaspard", "Missiaen", 21, "23"),
	entities.NewStudent(2, "Daurian", "Gauron", 20, "Go"),
	entities.NewStudent(3, "Daryl", "Caruso", 20, "-2"),
	entities.NewStudent(4, "Christopher", "Lessirard", 20, "26"),
	entities.NewStudent(5, "Nathan", "Da Silva Neves", 20, "26"),
}

func (dao StudentDAOMem) FindAll() []entities.Student {
	sort.SliceStable(students, func(i, j int) bool {
		return students[i].Id < students[j].Id
	})

	return students
}

func (dao StudentDAOMem) Find(id int) *entities.Student {
	for _, student := range students {
		if student.Id == id {
			return &student
		}
	}
	return nil
}

func (dao StudentDAOMem) Exists(id int) bool {
	for _, student := range students {
		if student.Id == id {
			return true
		}
	}
	return false
}

func (dao StudentDAOMem) Delete(id int) bool {
	for index, student := range students {
		if student.Id == id {
			students = append(students[:index], students[index+1:]...)
			return true
		}
	}
	return false
}

func (dao StudentDAOMem) Create(student entities.Student) bool {
	if dao.Exists(student.Id) {
		return false
	}
	students = append(students, student)
	return true
}

func (dao StudentDAOMem) Update(student entities.Student) bool {
	for index, stu := range students {
		if stu.Id == student.Id {
			students[index] = student
			return true
		}
	}
	return false
}
