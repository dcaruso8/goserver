package provider

import (
	p "internal/persistence"
	bolt "internal/persistence/daoBolt"
	mem "internal/persistence/daoMem"
)

var UseMemory bool = false

func NewStudentDAO() p.StudentDAO {
	if UseMemory {
		return mem.NewStudentDAOMem()
	} else {
		return bolt.NewStudentDAOBolt()
	}
}

func NewLanguageDAO() p.LanguageDAO {
	if UseMemory {
		return mem.NewLanguageDAOMem()
	} else {
		return bolt.NewLanguageDAOBolt()
	}
}
