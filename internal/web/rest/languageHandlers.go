package rest

import (
	"encoding/json"
	"fmt"
	"internal/entities"
	persist "internal/persistence"
	provider "internal/persistence/provider"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

var languageDao persist.LanguageDAO = provider.NewLanguageDAO()

// GetAllLanguages godoc
// @Summary Get all languages
// @Description get all languages
// @Tags Languages
// @Accept json
// @Produce json
// @Success 200 {array} entities.Language
// @Failure 404 {object} object
// @Router /languages [get]
func GetLanguageByCode(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	code := vars["code"]

	lang := languageDao.Find(code)

	if lang != nil {
		res, _ := json.Marshal(lang)
		fmt.Fprintf(w, "%s", res)
	} else {
		http.Error(w, "Could not find language with specified code", http.StatusNotFound)
	}

}

// GetAllLanguages ... Get all languages godoc
// @Summary Get all languages
// @Description get all languages
// @Tags Languages
// @Accept json
// @Produce json
// @Success 200 {array} entities.Language
// @Failure 404 {object} object
// @Router /languages [get]

func GetAllLanguages(w http.ResponseWriter, r *http.Request) {
	res, _ := json.Marshal(languageDao.FindAll())
	fmt.Fprintf(w, "%s", res)
}

func CreateLanguage(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var lang entities.Language

	json.Unmarshal(body, &lang)
	languageCreated := languageDao.Create(lang)

	if languageCreated == false {
		http.Error(w, "Could not create new language, because request is wrong or language already exists", http.StatusBadRequest)
	} else {
		json.NewEncoder(w).Encode(languageDao.FindAll())
	}
}

func DeleteLanguageByCode(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	code := vars["code"]

	languageDeleted := languageDao.Delete(code)

	if languageDeleted == false {
		http.Error(w, "Could not delete language, because doesn't exist", http.StatusNotFound)
	} else {
		json.NewEncoder(w).Encode(languageDao.FindAll())
	}
}

func UpdateLanguage(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var language entities.Language
	json.Unmarshal(body, &language)

	languageUpdated := languageDao.Update(language)

	if languageUpdated == false {
		http.Error(w, "Could not update language, because doesn't exist", http.StatusNotFound)
	} else {
		json.NewEncoder(w).Encode(languageDao.FindAll())
	}
}

//lgu.univ@gmail.com
