package rest

import (
	"encoding/json"
	"fmt"
	"internal/entities"
	persist "internal/persistence"
	provider "internal/persistence/provider"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var studentDao persist.StudentDAO = provider.NewStudentDAO()

func GetStudentById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err == nil {
		student := studentDao.Find(id)
		if student != nil {
			res, _ := json.Marshal(student)
			fmt.Fprintf(w, "%s", res)
		} else {
			http.Error(w, "Could not find student with specified id", http.StatusNotFound)
		}
	}
}

func GetAllStudents(w http.ResponseWriter, r *http.Request) {
	students := studentDao.FindAll()
	res, _ := json.Marshal(students)
	fmt.Fprintf(w, "%s", res)
}

func CreateStudent(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var student entities.Student

	json.Unmarshal(body, &student)
	studentCreated := studentDao.Create(student)

	if studentCreated == false {
		http.Error(w, "Could not create new student, because request is wrong or student already exists", http.StatusBadRequest)
	} else {
		json.NewEncoder(w).Encode(studentDao.FindAll())
	}
}

func DeleteStudentById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err == nil {
		studentDeleted := studentDao.Delete(id)

		if studentDeleted == false {
			http.Error(w, "Could not delete student, because doesn't exist", http.StatusNotFound)
		} else {
			json.NewEncoder(w).Encode(studentDao.FindAll())
		}
	}
}

func UpdateStudent(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var student entities.Student
	json.Unmarshal(body, &student)

	studentUpdated := studentDao.Update(student)

	if studentUpdated == false {
		http.Error(w, "Could not update student, because doesn't exist", http.StatusNotFound)
	} else {
		json.NewEncoder(w).Encode(studentDao.FindAll())
	}
}
