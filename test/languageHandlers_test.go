package test

import (
	"bytes"
	"encoding/json"
	"internal/entities"
	"internal/web/rest"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

//GET
func TestGetAllLanguages(t *testing.T) {
	req, err := http.NewRequest("GET", "/languages", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.GetAllLanguages)
	handler.ServeHTTP(rr, req)
	checkResponseCode(t, http.StatusOK, rr.Code)

	// Check the response body is what we expect.
	expectedLanguages := []entities.Language{
		entities.NewLanguage("Fr", "Français"), entities.NewLanguage("En", "Anglais"),
	}

	out, _ := json.Marshal(expectedLanguages)
	if !strings.ContainsAny(rr.Body.String(), string(out)) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedLanguages)
	}
}

func TestGetLanguageByCode(t *testing.T) {
	var expectedLanguage entities.Language = entities.NewLanguage("Fr", "Français")

	resp, err := http.Get("http://localhost:8080/languages/Fr/")

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		var lang entities.Language
		json.Unmarshal(body, &lang)

		checkResponseCode(t, http.StatusOK, resp.StatusCode)
		assert.Equal(t, lang, expectedLanguage)
	}
}

func TestGetLanguageByCode_Fail(t *testing.T) {
	resp, err := http.Get("http://localhost:8080/languages/It/")

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		checkResponseCode(t, http.StatusNotFound, resp.StatusCode)
		var lang entities.Language
		json.Unmarshal(body, &lang)

		assert.Empty(t, lang)

	}
}

//POST
func TestCreateLanguage(t *testing.T) {
	var createdLanguage entities.Language = entities.NewLanguage("Jp", "Japonais")
	out, _ := json.Marshal(createdLanguage)

	req, err := http.NewRequest("POST", "/languages", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.CreateLanguage)
	handler.ServeHTTP(rr, req)
	checkResponseCode(t, http.StatusOK, rr.Code)

	var languages []entities.Language
	json.Unmarshal(rr.Body.Bytes(), &languages)

	assert.Contains(t, languages, createdLanguage)
}
func TestCreateExistingLanguage_Fail(t *testing.T) {
	var createdLanguage entities.Language = entities.NewLanguage("Fr", "Français")
	out, _ := json.Marshal(createdLanguage)

	req, err := http.NewRequest("POST", "/languages", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.CreateLanguage)
	handler.ServeHTTP(rr, req)
	checkResponseCode(t, http.StatusBadRequest, rr.Code)
}

//PUT
func TestUpdateLanguage(t *testing.T) {
	var updatedLanguage entities.Language = entities.NewLanguage("Fr", "French")
	out, _ := json.Marshal(updatedLanguage)

	req, err := http.NewRequest("PUT", "/languages", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.UpdateLanguage)
	handler.ServeHTTP(rr, req)

	checkResponseCode(t, http.StatusOK, rr.Code)

	var languages []entities.Language
	json.Unmarshal(rr.Body.Bytes(), &languages)

	assert.Contains(t, languages, updatedLanguage)
}

func TestUpdateUnkownLanguage_Fail(t *testing.T) {
	var updatedLanguage entities.Language = entities.NewLanguage("It", "Italian")
	out, _ := json.Marshal(updatedLanguage)

	req, err := http.NewRequest("PUT", "/languages", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.UpdateLanguage)
	handler.ServeHTTP(rr, req)

	checkResponseCode(t, http.StatusNotFound, rr.Code)
}

//DELETE
func TestDeleteLanguage(t *testing.T) {
	var deletedLanguage entities.Language = entities.NewLanguage("En", "Anglais")

	client := &http.Client{}
	req, err := http.NewRequest(http.MethodDelete, "http://localhost:8080/languages/En/", nil)
	if err != nil {
		// handle error
		log.Fatal(err)
	}

	resp, err := client.Do(req)

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		checkResponseCode(t, http.StatusOK, resp.StatusCode)

		var languages []entities.Language
		json.Unmarshal(body, &languages)
		assert.NotContains(t, languages, deletedLanguage)
	}
}
func TestDeleteUnkownLanguage(t *testing.T) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodDelete, "http://localhost:8080/languages/It/", nil)
	if err != nil {
		// handle error
		log.Fatal(err)
	}

	resp, err := client.Do(req)

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		checkResponseCode(t, http.StatusNotFound, resp.StatusCode)
	}
}
