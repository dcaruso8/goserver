package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"internal/entities"
	"internal/web/rest"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

//GET
func TestGetAllStudents(t *testing.T) {
	req, err := http.NewRequest("GET", "/students", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.GetAllStudents)
	handler.ServeHTTP(rr, req)
	checkResponseCode(t, http.StatusOK, rr.Code)

	// Check the response body is what we expect.
	var expectedStudents []entities.Student = []entities.Student{
		entities.NewStudent(1, "Gaspard", "Missiaen", 21, "23"),
		entities.NewStudent(2, "Daurian", "Gauron", 20, "Go"),
		entities.NewStudent(3, "Daryl", "Caruso", 20, "-2"),
		entities.NewStudent(4, "Christopher", "Lessirard", 20, "26"),
		entities.NewStudent(5, "Nathan", "Da Silva Neves", 20, "26"),
	}

	out, _ := json.Marshal(expectedStudents)
	fmt.Println(rr.Body)
	fmt.Println(string(out))
	if !strings.ContainsAny(rr.Body.String(), string(out)) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expectedStudents)
	}
}

func TestGetStudentById(t *testing.T) {
	var expectedStudent entities.Student = entities.NewStudent(1, "Gaspard", "Missiaen", 21, "23")

	resp, err := http.Get("http://localhost:8080/students/1/")

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		var student entities.Student
		json.Unmarshal(body, &student)

		checkResponseCode(t, http.StatusOK, resp.StatusCode)
		assert.Equal(t, student, expectedStudent)
	}
}

func TestGetSudentById_Fail(t *testing.T) {
	resp, err := http.Get("http://localhost:8080/students/8888888888/")

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		checkResponseCode(t, http.StatusNotFound, resp.StatusCode)
		var student entities.Student
		json.Unmarshal(body, &student)

		assert.Empty(t, student)

	}
}

//POST
func TestCreateStudent(t *testing.T) {
	var createdStudent entities.Student = entities.NewStudent(88, "Louan", "Geray", 19, "23")
	out, _ := json.Marshal(createdStudent)

	req, err := http.NewRequest("POST", "/students", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.CreateStudent)
	handler.ServeHTTP(rr, req)
	checkResponseCode(t, http.StatusOK, rr.Code)

	var students []entities.Student
	json.Unmarshal(rr.Body.Bytes(), &students)

	assert.Contains(t, students, createdStudent)
}

func TestCreateExistingStudent_Fail(t *testing.T) {
	var createdStudent entities.Student = entities.NewStudent(1, "Gaspard", "Missiaen", 21, "23")
	out, _ := json.Marshal(createdStudent)

	req, err := http.NewRequest("POST", "/students", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.CreateStudent)
	handler.ServeHTTP(rr, req)
	checkResponseCode(t, http.StatusBadRequest, rr.Code)
}

//PUT
func TestUpdateStudent(t *testing.T) {
	var updatedStudent entities.Student = entities.NewStudent(1, "Gaspard", "Jsp", 21, "23")
	out, _ := json.Marshal(updatedStudent)

	req, err := http.NewRequest("PUT", "/students", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.UpdateStudent)
	handler.ServeHTTP(rr, req)

	checkResponseCode(t, http.StatusOK, rr.Code)

	var students []entities.Student
	json.Unmarshal(rr.Body.Bytes(), &students)

	assert.Contains(t, students, updatedStudent)
}

func TestUpdateUnkownStudent_Fail(t *testing.T) {
	var updatedStudent entities.Student = entities.NewStudent(88888888888888, "Gaspard", "Jsp", 21, "23")
	out, _ := json.Marshal(updatedStudent)

	req, err := http.NewRequest("PUT", "/students", bytes.NewBuffer(out))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(rest.UpdateStudent)
	handler.ServeHTTP(rr, req)

	checkResponseCode(t, http.StatusNotFound, rr.Code)
}

//DELETE
func TestDeleteStudent(t *testing.T) {
	var deletedStudent entities.Student = entities.NewStudent(4, "Christopher", "Lessirard", 20, "26")

	client := &http.Client{}
	req, err := http.NewRequest(http.MethodDelete, "http://localhost:8080/students/4/", nil)
	if err != nil {
		// handle error
		log.Fatal(err)
	}

	resp, err := client.Do(req)

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		checkResponseCode(t, http.StatusOK, resp.StatusCode)

		var students []entities.Student
		json.Unmarshal(body, &students)
		assert.NotContains(t, students, deletedStudent)
	}
}
func TestDeleteUnkownStudent(t *testing.T) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodDelete, "http://localhost:8080/students/998/", nil)
	if err != nil {
		// handle error
		log.Fatal(err)
	}

	resp, err := client.Do(req)

	if err != nil {
		t.Fatal(err)
	} else {
		defer resp.Body.Close()
		checkResponseCode(t, http.StatusNotFound, resp.StatusCode)
	}
}
