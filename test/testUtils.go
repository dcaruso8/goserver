package test

import (
	"encoding/json"
	"internal/entities"
	"strings"
	"testing"
)

//-----------------------fonctions utilitaires
func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

//converti du json en tableau de langages
func JsonToLanguagesArray(body string) []entities.Language {
	var languages []entities.Language

	ss := strings.Replace(string(body), "[", "", -1)
	ss = strings.Replace(ss, "]", "", -1)
	split := strings.SplitAfter(ss, "},")

	for _, s := range split {
		endChar := string(s[len(s)-1])

		if endChar == "," { //si la ligne se termine par ','
			s = strings.TrimRight(s, ",")
		}

		var lang entities.Language
		json.Unmarshal([]byte(s), &lang)

		languages = append(languages, lang)
	}

	return languages
}
